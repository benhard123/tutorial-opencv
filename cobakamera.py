import cv2

cap = cv2.VideoCapture(0) #nangkap kamera

while True:
    ret,frame = cap.read() #ambil frame dari kamera
    cv2.imshow('frame',frame) #nunjukin ke window
    if cv2.waitKey(1) & 0xFF == ord('q'): #keluar dengan huruf q
        break

cap.release()
cv2.destroyAllWindows()

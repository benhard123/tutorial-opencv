import cv2

#range hsv bola
bolaLower=(1, 100, 100)
bolaUpper=(50, 255, 255)

cap = cv2.VideoCapture(0)


while True:
    #mengambil gambar dari kamera
    ret,frame = cap.read()
    #mengubah gambar ke hsv
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # masking warna di dalam range hsv
    mask = cv2.inRange(hsv, bolaLower, bolaUpper)

    # erode untuk menghilangkan noise
    mask = cv2.erode(mask, None, iterations=5)

    #dilate untuk melebarkan maskin
    mask = cv2.dilate(mask, None, iterations=15)

    #mendapatkan countour dari mask
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    
    #menampilkan mask
    cv2.imshow('mask',mask)

    #jika terdapat contour
    if len(cnts)>0 : 
        # find the largest contour in the mask, then use 
        # it to compute the minimum enclosing circle and 
        # centroid 
        c = max(cnts, key=cv2.contourArea) 
        ((x, y), radius) = cv2.minEnclosingCircle(c) 
        M = cv2.moments(c) 
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"])) 
        # only proceed if the radius meets a minimum size 
        if radius > 10: 
            # draw the circle and centroid on the frame, 
            # then update the list of tracked points 
            cv2.circle(frame, (int(x), int(y)), int(radius), (80, 127, 255), 2)  
            cv2.circle(frame, center, 5, (0, 0, 255), -1)

    cv2.imshow('frame',frame)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
